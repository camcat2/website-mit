---
title: Static stress triggering in operational earthquake forecasting
subtitle: with Sebastian Hainzl and the Collaboratory for the Study of Earthquake Predictability (CSEP)
tags:
#- past
date: "2016-04-27T00:00:00Z"
weight: 80


# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Maps of daily forecast in the first and second day after the Darfield mainshock. From [*Cattania et al., 2018*](/publication/cattania-2018) 
  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#rl_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

Aftershock sequences take place after all moderate and large earthquakes, and are a significant source of hazard. A primary mechanism for aftershocks is the static deformation and stresses in vicinity of a mainshock. Physics-based models for Operational Earthquake Forecasting bring our physical knowledge of elasticity and friction into time-dependent, probabilistic earthquake forecasts.

Our approach has been guided by an attempt to construct physically consistent and realistic models of the processes involved, by including time-dependent (aseismic) fault slip and a realistic fault geometry. We found that deep afterslip following large subduction earthquakes significantly contributes to triggering seismicity on shallow crustal faults ([Cattania et al, 2015](https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1002/2014JB011500)). Another outcome of this work is that stress heterogeneity due to the geometrical complexity of a fault system has a first-order impact in model behavior ([Cattania et al, 2014](https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1002/2014JB011183)), and it significantly improves performance. 
This improvement was confirmed by a collaborative experiment carried out within CSEP for the aftershock sequence of the 2010 Canterbury (New Zealand) earthquake ([Cattania et al., 2018](https://pubs.geoscienceworld.org/ssa/srl/article-abstract/89/4/1238/532042/the-forecasting-skill-of-physics-based-seismicity?redirectedFrom=PDF)). In a collaboration with Simone Mancini (British Geological Survey/University of Bristol), Margarita Segou (BGS) and Max Werner (Univeristy of Bristol) we have further improved and tested these models, in the context of [the 2016-2017 sequence in the Central Appennines (Italy)](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2019JB017874).

External links: [*International collaboration studies the predictability of earthquakes*](https://phys.org/news/2018-06-international-collaboration-earthquakes.html) on phys.org
