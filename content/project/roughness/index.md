---
title: Seismicity on rough faults
summary: Foreshocks and aseismic slip modulated by normal stress heterogeneity on rough faults
tags:
#- current
date: "2016-04-27T00:00:00Z"
weight: 20

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Foreshocks and accelerating creep on a simulated fractal fault, from *Cattania and Segall, (under review), Precursory slow slip and foreshocks on rough faults* ([link to preprint](https://eartharxiv.org/9xphk/)).

  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

Faults are not planar features, but instead exhibit geometrical roughness at all scales. This affects the slip behavior of individual ruptures, as well as the seismicity patterns. Our goal is to understand how fault roughness affects seismic sequences.

We have been exploring this question with quasi-dynamic rate-state simulations. The effect of roughness is easily understood in terms of normal stress variations, as follows. Slip on a rough fault generates regions of compressive stresses, which are stronger and tend to act as seismic asperities: they are locked between earthquakes, and once they accelerate they break in fast (seismic) events. Other regions experience tensile stress perturbations due to slip, and these weaker regions tend to slip in a stable matter (creep). Foreshock sequences are controlled by the stress transfer between these two regions, and several observed features (such as the temporal evolution of foreshocks and their tendency to migrate) arise naturally from these interactions.

In future work we will explore how a rough fault responds to external stress perturbations such as those from regional and remote earthquakes or anthropogenic activites (e.g. fluid injection). We will also extend our simulations to 3-D and include inelastic effects.

Preprint: [*Precursory slow slip and foreshocks on rough faults*](https://eartharxiv.org/9xphk/)

