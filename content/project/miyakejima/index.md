---
title: Seismic swarms and aseismic slip driven by dikes
summary: In a seismic swarm during the 2000 Miyakejima dike intrusion we find evidence of aseismic slip on a complex fault system.
tags:
#- past
date: "2016-04-27T00:00:00Z"

weight: 90

# Optional external URL for project (replaces project detail page).
external_link: "publication/cattania-2017-d/"

image:
  caption: Seismic swarms during the 2000 Miyakejima dike intrusion, from *Cattania et al. (2015)* [**]()
  focal_point: Smart
  
links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

In addition to static stress changes from earthquakes on nearby faults, seismic sequences can be triggered by stress perturbations due to a number of processes, including magma migration and seismic waves from remote earthquakes. 

With Eleonora Rivalta, Luigi Passarelli and Yosuke Aoki, we analyzed a seismic swarm during the 2000 Miyakejima dike intrusion and found evidence of aseismic slip on a complex fault system. 

With Jeff McGuire and John Collins, we used a seismic catalog from ocean bottom seismometers in the East Pacific Rise, and detected instances of dynamic earthquake triggering from remote mainshocks. 


